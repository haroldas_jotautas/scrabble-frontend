import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import {
  RequestOptions,
} from "@angular/http";
import { UserParams } from '../providers/dto';
import { DataService } from '../providers/data-service';
import 'rxjs/add/operator/map';

/*
  Generated class for the ApiService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/

const WEBSERVICES_PORT = '8080';
const WEBSERVICES_IP = 'http://localhost';

//const WEBSERVICES_PORT = '8081';
//const WEBSERVICES_IP = 'http://ec2-35-166-30-88.us-west-2.compute.amazonaws.com';

var REGISTER: any = WEBSERVICES_IP + ':' + WEBSERVICES_PORT + '/api/users/';
var LOGIN: any = WEBSERVICES_IP + ':' + WEBSERVICES_PORT + '/api/auth/';
var GET_GAMES: any = WEBSERVICES_IP + ':' + WEBSERVICES_PORT + '/api/games/';
var GET_GAMES_FINISHED: any = WEBSERVICES_IP + ':' + WEBSERVICES_PORT + '/api/games/finished';
var GET_GAMES_IN_PROGRESS: any = WEBSERVICES_IP + ':' + WEBSERVICES_PORT + '/api/games/inProgress';
var GET_GAME: any = WEBSERVICES_IP + ':' + WEBSERVICES_PORT + '/api/games/';
var NEW_GAME: any = WEBSERVICES_IP + ':' + WEBSERVICES_PORT + '/api/games';
var PLACE_WORD: any = WEBSERVICES_IP + ':' + WEBSERVICES_PORT + '/api/moves/';
var SKIP_TURN: any = WEBSERVICES_IP + ':' + WEBSERVICES_PORT + '/api/moves/skip/';
var START_SEARCH: any = WEBSERVICES_IP + ':' + WEBSERVICES_PORT + '/api/games/start';

@Injectable()
export class ApiService {

  constructor(public http: Http, public dataService: DataService) {

  }

  public register(params: UserParams) {
    return new Promise(resolve => {
      console.log('register');
      this.http.post(REGISTER, params)
      .subscribe(data => {
          console.log('data', data);
          resolve(data);
      });
    });
  }

  public login(user) {
    console.log('login');
    return new Promise(resolve => {
      this.http.post(LOGIN, user)
      .map(res => res.json())
      .subscribe(login => {
        this.dataService.login(user.email, login['token']);
        resolve(login);
      });
    });

  }

  public getGames() {
    return this.dataService.getToken().then((data) => {
      var token = data;
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('authorization', 'Bearer ' + token);

      return new Promise(resolve => {
        this.http.get(GET_GAMES, {headers: headers})
          .map(res => res.json())
          .subscribe(games => {
            console.log('games', games);
            resolve(games);
          });
        });
    });
  }

  public getGamesFinished() {
    return this.dataService.getToken().then((data) => {
      var token = data;
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('authorization', 'Bearer ' + token);

      return new Promise(resolve => {
        this.http.get(GET_GAMES_FINISHED, {headers: headers})
          .map(res => res.json())
          .subscribe(games => {
            console.log('games finished', games);
            resolve(games);
          });
        });
    });
  }

    public getGamesInProgress() {
    return this.dataService.getToken().then((data) => {
      var token = data;
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('authorization', 'Bearer ' + token);

      return new Promise(resolve => {
        this.http.get(GET_GAMES_IN_PROGRESS, {headers: headers})
          .map(res => res.json())
          .subscribe(games => {
            console.log('games inProgress', games);
            resolve(games);
          });
        });
    });
  }

  public placeWord(gameId, word){
    return this.dataService.getToken().then((data) => {
      var token = data;
      let options = {};
      let newOptions = new RequestOptions({}).merge(options);
      let newHeaders = new Headers(newOptions.headers);
      newHeaders.append('Content-Type', 'application/json');
      newHeaders.append('authorization', 'Bearer ' + token);
      newOptions.headers = newHeaders;

      return new Promise(resolve => {
        this.http.post(PLACE_WORD + gameId, word, newOptions)
        .map(res => res.json())
        .subscribe(data => {
          console.log('place word', data);
              resolve(data);
            });
      });
    });
  }

  public skipTurn(gameId){
    return this.dataService.getToken().then((data) => {
      var token = data;
      let options = {};
      let newOptions = new RequestOptions({}).merge(options);
      let newHeaders = new Headers(newOptions.headers);
      newHeaders.append('Content-Type', 'application/json');
      newHeaders.append('authorization', 'Bearer ' + token);
      newOptions.headers = newHeaders;

      return new Promise(resolve => {
        this.http.post(SKIP_TURN + gameId, {}, newOptions)
        .subscribe(data => {
          console.log('skip turn', data);
              resolve(data);
            });
      });
    });
  }

  public getGame(gameId) {
    return this.dataService.getToken().then((data) => {
      var token = data;
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('authorization', 'Bearer ' + token);

      return new Promise(resolve => {
        this.http.get(GET_GAME + '/' + gameId, {headers: headers})
          .map(res => res.json())
          .subscribe(games => {
            console.log('games', games);
            resolve(games);
          });
        });
    });
  }

  public newGame(){
    return this.dataService.getToken().then((data) => {
      console.log('token', data);
      var token = data;
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('authorization', 'Bearer ' + token);

      return new Promise(resolve => {
        this.http.post(NEW_GAME, {}, {headers: headers})
        .map(res => res.json())
        .subscribe(data => {
          console.log('new game', data);
              resolve(data);
            });
      });
    });
  }

  public startSearch(playerName){
    return this.dataService.getToken().then((data) => {
      var token = data;
      let options = {};
      let newOptions = new RequestOptions({}).merge(options);
      let newHeaders = new Headers(newOptions.headers);
      newHeaders.append('Content-Type', 'application/json');
      newHeaders.append('authorization', 'Bearer ' + token);
      newOptions.headers = newHeaders;

      return new Promise(resolve => {
        this.http.post(START_SEARCH, playerName, newOptions)
        .map(res => res.json())
        .subscribe(data => {
          console.log('start', data);
              resolve(data);
            });
      });
    });
  }

}
