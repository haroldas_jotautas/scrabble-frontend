import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

/*
  Generated class for the Utils provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class Utils {

  constructor() {
    console.log('Hello Utils Provider');
  }

  getAllLetters() : string[]{
    let letters: string[] = [
      'A', 'Ą', 'B', 'C', 'Č', 'D', 'E', 'Ę', 'Ė', 'F', 'G',
      'H', 'I', 'Į', 'Y', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
      'R', 'S', 'Š', 'T', 'U', 'Ų', 'Ū', 'V', 'Z', 'Ž', ];
    return letters;
  }

  getPointValue(letter){
    if(!letter)
      return 0;
      
    let value = 0;
      switch(letter.toLowerCase()){
        case "a":
            value = 1;
            break;
        case "ą":
            value = 8;
            break;
        case "b":
            value = 2;
            break;
        case "c":
            value = 10;
            break;
        case "č":
            value = 8;
            break;
        case "d":
            value = 2;
            break;
        case "e":
            value = 1;
            break;
        case "ę":
            value = 10;
            break;
        case "ė":
            value = 4;
            break;
        case "f":
            value = 10;
            break;
        case "g":
            value = 4;
            break;
        case "h":
            value = 10;
            break;
        case "i":
            value = 1;
            break;
        case "į":
            value = 8;
            break;
        case "y":
            value = 5;
            break;
        case "j":
            value = 4;
            break;
        case "k":
            value = 1;
            break;
        case "l":
            value = 2;
            break;
        case "m":
            value = 2;
            break;
        case "n":
            value = 1;
            break;
        case "o":
            value = 1;
            break;
        case "p":
            value = 3;
            break;
        case "r":
            value = 1;
            break;
        case "s":
            value = 1;
            break;
        case "š":
            value = 5;
            break;
        case "t":
            value = 1;
            break;
        case "u":
            value = 1;
            break;
        case "ų":
            value = 6;
            break;
        case "ū":
            value = 8;
            break;
        case "v":
            value = 4;
            break;
        case "z":
            value = 10;
            break;
        case "ž":
            value = 6;
            break;
      }

      return value;
  }

}
