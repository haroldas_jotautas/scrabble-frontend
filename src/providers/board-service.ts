import { Injectable } from '@angular/core';

/*
  Generated class for the BoardService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class BoardService {

  board: any = [
    [{"type": "TŽ"},{"type": ""},{"type": ""},{"type": "DR"},{"type": ""},{"type": ""},{"type": ""},{"type": "TŽ"},{"type": ""},{"type": ""},{"type": ""},{"type": "DR"},{"type": ""},{"type": ""},{"type": "TŽ"}],
    [{"type": ""},{"type": "DŽ"},{"type": ""},{"type": ""},{"type": ""},{"type": "TR"},{"type": ""},{"type": ""},{"type": ""},{"type": "TR"},{"type": ""},{"type": ""},{"type": ""},{"type": "DŽ"},{"type": ""}],
    [{"type": ""},{"type": ""},{"type": "DŽ"},{"type": ""},{"type": ""},{"type": ""},{"type": "DR"},{"type": ""},{"type": "DR"},{"type": ""},{"type": ""},{"type": ""},{"type": "DŽ"},{"type": ""},{"type": ""}],
    [{"type": "DR"},{"type": ""},{"type": ""},{"type": "DŽ"},{"type": ""},{"type": ""},{"type": ""},{"type": "DR"},{"type": ""},{"type": ""},{"type": ""},{"type": "DŽ"},{"type": ""},{"type": ""},{"type": "DR"}],
    [{"type": ""},{"type": ""},{"type": ""},{"type": ""},{"type": "DŽ"},{"type": ""},{"type": ""},{"type": ""},{"type": ""},{"type": ""},{"type": "DŽ"},{"type": ""},{"type": ""},{"type": ""},{"type": ""}],
    [{"type": ""},{"type": "TR"},{"type": ""},{"type": ""},{"type": ""},{"type": "TR"},{"type": ""},{"type": ""},{"type": ""},{"type": "TR"},{"type": ""},{"type": ""},{"type": ""},{"type": "TR"},{"type": ""}],
    [{"type": ""},{"type": ""},{"type": "DR"},{"type": ""},{"type": ""},{"type": ""},{"type": "DR"},{"type": ""},{"type": "DR"},{"type": ""},{"type": ""},{"type": ""},{"type": "DR"},{"type": ""},{"type": ""}],
    [{"type": "TŽ"},{"type": ""},{"type": ""},{"type": "DR"},{"type": ""},{"type": ""},{"type": ""},{"type": "DŽ"},{"type": ""},{"type": ""},{"type": ""},{"type": "DR"},{"type": ""},{"type": ""},{"type": "TŽ"}],
    [{"type": ""},{"type": ""},{"type": "DR"},{"type": ""},{"type": ""},{"type": ""},{"type": "DR"},{"type": ""},{"type": "DR"},{"type": ""},{"type": ""},{"type": ""},{"type": "DR"},{"type": ""},{"type": ""}],
    [{"type": ""},{"type": "TR"},{"type": ""},{"type": ""},{"type": ""},{"type": "TR"},{"type": ""},{"type": ""},{"type": ""},{"type": "TR"},{"type": ""},{"type": ""},{"type": ""},{"type": "TR"},{"type": ""}],
    [{"type": ""},{"type": ""},{"type": ""},{"type": ""},{"type": "DŽ"},{"type": ""},{"type": ""},{"type": ""},{"type": ""},{"type": ""},{"type": "DŽ"},{"type": ""},{"type": ""},{"type": ""},{"type": ""}],
    [{"type": "DR"},{"type": ""},{"type": ""},{"type": "DŽ"},{"type": ""},{"type": ""},{"type": ""},{"type": "DR"},{"type": ""},{"type": ""},{"type": ""},{"type": "DŽ"},{"type": ""},{"type": ""},{"type": "DR"}],
    [{"type": ""},{"type": ""},{"type": "DŽ"},{"type": ""},{"type": ""},{"type": ""},{"type": "DR"},{"type": ""},{"type": "DR"},{"type": ""},{"type": ""},{"type": ""},{"type": "DŽ"},{"type": ""},{"type": ""}],
    [{"type": ""},{"type": "DŽ"},{"type": ""},{"type": ""},{"type": ""},{"type": "TR"},{"type": ""},{"type": ""},{"type": ""},{"type": "TR"},{"type": ""},{"type": ""},{"type": ""},{"type": "DŽ"},{"type": ""}],
    [{"type": "TŽ"},{"type": ""},{"type": ""},{"type": "DR"},{"type": ""},{"type": ""},{"type": ""},{"type": "TŽ"},{"type": ""},{"type": ""},{"type": ""},{"type": "DR"},{"type": ""},{"type": ""},{"type": "TŽ"}]
  ];

  constructor() {
    console.log('Hello BoardService Provider');
  }

  getBoard(){
    return this.board;
  }



}
