import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Events } from 'ionic-angular';
import localforage from 'localforage';

/*
  Generated class for the DataService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class DataService {
  HAS_LOGGED_IN = 'hasLoggedIn';

  constructor(public events: Events) {

  }

  login(username, token) {
    localforage.setItem<any>(this.HAS_LOGGED_IN, true);
    this.setToken(token);
    this.setUsername(username);
    this.events.publish('user:login');
  }

  logout() {
    localforage.setItem<any>(this.HAS_LOGGED_IN, false);
    localforage.setItem<any>('id_token', null);
    this.events.publish('user:logout');
  }

  setToken(token) {
    localforage.setItem<any>('id_token', token);
  }

  getToken() {
    return localforage.getItem<any>('id_token').then((value) => {
      return value;
    });
  }

  setUsername(username) {
    localforage.setItem<any>('username', username);
  }

  getUsername() {
    return localforage.getItem<any>('username').then((value) => {
      return value;
    });
  }

  hasLoggedIn() {
    return localforage.getItem(this.HAS_LOGGED_IN).then((value) => {
      return value;
    });
  }

}
