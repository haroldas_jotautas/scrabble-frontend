import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { ScrabblePage } from '../pages/scrabble/scrabble';
import { ItemDetailsPage } from '../pages/item-details/item-details';
import { ListPage } from '../pages/list/list';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { Utils } from '../providers/utils';
import { ApiService } from '../providers/api-service';
import { DataService } from '../providers/data-service';
import { BoardService } from '../providers/board-service';
import { Letter } from '../pipes/letter';
import { WildcardSelectPage } from '../pages/wildcard-select/wildcard-select';

@NgModule({
  declarations: [
    MyApp,
    ScrabblePage,
    ItemDetailsPage,
    ListPage,
    LoginPage,
    HomePage,
    Letter,
    WildcardSelectPage,
    RegisterPage
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ScrabblePage,
    ItemDetailsPage,
    ListPage,
    LoginPage,
    HomePage,
    WildcardSelectPage,
    RegisterPage
  ],
  providers: [
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Utils,
    ApiService,
    DataService,
    BoardService
  ]
})
export class AppModule {}
