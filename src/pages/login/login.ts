import { Component } from '@angular/core';
import { NavController, AlertController, NavParams } from 'ionic-angular';
import { DataService } from '../../providers/data-service';
import { ApiService } from '../../providers/api-service';
import { HomePage } from '../home/home'
import { RegisterPage } from '../register/register';

/*
  Generated class for the Login page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  email: any;
  password: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public dataService: DataService,
    public alertCtrl: AlertController,
    public apiService : ApiService) {}

  ionViewDidLoad() {
    this.dataService.getUsername().then((username) =>{
      this.email = username;
    });
  }

  login(){
      let user = {
          email: this.email,
          password: this.password
      };

      this.apiService.login(user)
        .then((login) => {
          this.navCtrl.setRoot(HomePage, {
            email: this.email
          });
        }, e => this.handleError(e)
      );
  }

  register(){
    this.navCtrl.push(RegisterPage);
  }

  handleError(error) {
    switch (error.status) {
      case 401:
        console.log('Wrong email or password.');
        let alert = this.alertCtrl.create({
          title: 'Oops!',
          subTitle: 'Wrong username or password.',
          buttons: ['Ok']
        });
        alert.present();
    }
  }

}
