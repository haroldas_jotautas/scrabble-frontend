import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { DataService } from '../../providers/data-service';
import { ApiService } from '../../providers/api-service';
import { LoginPage } from '../login/login'
import { ScrabblePage } from '../scrabble/scrabble';
import { Utils } from '../../providers/utils';

/*
  Generated class for the Home page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  games: any = [];
  gamesFinished: any = [];
  tab: String = 'progress';

  searchPlayer: String = '';
  searchOn: Boolean = false;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public dataService : DataService,
    public apiService : ApiService,
    public alertCtrl: AlertController,
    public utils: Utils
    ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
    this.loadGames(null);
  }

    doRefresh(refresher){
      this.loadGames(refresher);
    }

  loadGames(refresher){
    Promise.all([    this.apiService.getGamesInProgress().then((games) => {
      this.games = games;
      this.generateRecentMoves();
    }),    this.apiService.getGamesFinished().then((games) => {
      this.gamesFinished = games;
    })]).then(() => {
      if(refresher){
        refresher.complete();
      }
    });
  }

  logout(){
    this.dataService.logout();
    this.navCtrl.setRoot(LoginPage);
  }

  openGame(gameId){
    this.navCtrl.push(ScrabblePage, {
      gameId: gameId
    });
  }

  newGame(){
    this.apiService.newGame().then(() => {
      this.loadGames(null);
    });
  }

  startSearch(){
    this.apiService.startSearch(this.searchPlayer).then((data) => {
      console.log('searched', data);
      if(data['error']){
        let alert = this.alertCtrl.create({
          title: 'Klaida',
          subTitle: data['errorMessage'],
          buttons: ['Ok']
        });
        alert.present();
      }else{
        this.openGame(data['id']);
      }
    });
  }

  generateRecentMoves(){
    for(let game of this.games){
      if(game.player2RecentMoves != null && game.player2RecentMoves[0] != null){
        game.player2LastMove = game.player2RecentMoves[0].word.split('');
      }
      if(game.player1RecentMoves != null && game.player1RecentMoves[0] != null){
        game.player1LastMove = game.player1RecentMoves[0].word.split('');
        
      }
    }
  }

  getPointValue(letter){
    let points = this.utils.getPointValue(letter);
    if(points != 0)
      return points + '';
    else
      return "";
  }

}
