import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { AuthService } from '../../providers/auth/auth';
import { LoginPage } from '../login/login';
import { ApiService } from '../../providers/api-service';
import { UserParams } from "../../providers/dto";

/*
  Generated class for the RegisterPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-register',
  templateUrl: 'register.html'
})
export class RegisterPage {

  email: any;
  username: any;
  password: any;

  constructor(private navCtrl: NavController, private toastCtrl: ToastController,
    public apiService : ApiService) {
  }

register(){
    let user: UserParams = {
      email: this.email,
      name: this.username,
      password: this.password
    }

    if(!user.email || !user.name || !user.password){
      let toast = this.toastCtrl.create({
        message: 'Reikia užpildyti visus laukus.',
        duration: 3000,
        position: 'bottom'
      });

      toast.present();
    }else{
      this.apiService.register(user).then((data) => {
        console.log(data);

        let toast = this.toastCtrl.create({
          message: data['Message'],
          duration: 3000,
          position: 'bottom'
        });

        toast.present();
        if(!data['Error']){
          this.navCtrl.setRoot(LoginPage);
        }

      }, (err) => {
          console.log('err: ' + err);
      });
    }

  }

}
