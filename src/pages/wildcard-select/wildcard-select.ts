import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { Utils } from '../../providers/utils';

/*
  Generated class for the ErrorModal page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-wildcard-select',
  templateUrl: 'wildcard-select.html'
})
export class WildcardSelectPage {

  letter : String = "";
  letters : String[];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public utils : Utils) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad WildcardSelectPage');
    this.letters = this.utils.getAllLetters();
  }

  selectLetter(letter){
    console.log('selectedLetter ' + letter);
    this.letter = letter;
    this.viewCtrl.dismiss(this.letter + "*");
  }

  dismissModal(){
    this.viewCtrl.dismiss();
  }

}
