import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, AlertController, Modal } from 'ionic-angular';
import { Utils } from '../../providers/utils';
import { BoardService } from '../../providers/board-service';
import { ApiService } from '../../providers/api-service';
import { WildcardSelectPage } from '../wildcard-select/wildcard-select'


@Component({
  selector: 'page-scrabble',
  templateUrl: 'scrabble.html'
})
export class ScrabblePage {

  boardLetters: any = null;

  game: any;
  board: any;

  deck: any = [];

  placedLetters : any = [];
  selectedCol: any;
  placeWordButtonColor: String = 'scrabble';
  placingWord: Boolean = false;
  skipingTurn: Boolean = false;

  constructor(
    public params: NavParams,
    public utils: Utils,
    public boardService : BoardService,
    public apiService : ApiService,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
    this.board = this.boardService.getBoard();
    this.loadGameData();
  }

  loadGameData(): void {
    this.apiService.getGame(this.params.get('gameId')).then((data) => {
      this.placedLetters = [];
      this.game = data;
      this.boardLetters = JSON.parse(this.game.board);
      console.log('this.game.board', this.game.board);
      console.log('boardLetters', this.boardLetters);
      this.deck = JSON.parse(this.game.playerDeck);
      if(this.game.playerTurn){
        this.placeWordButtonColor = 'scrabble';
      }else{
        this.placeWordButtonColor = 'darkSecondary';
      }
      console.log('game', this.game);
      console.log('deck ', this.deck );
    });
  }

  selectCol(col, i, j): void {
    let letter = this.boardLetters[i][j].letter;
    console.log('this.boardLetters[i][j]', this.boardLetters[i][j]);
    if(letter == ' '){
      if(this.selectedCol){
        this.selectedCol.hover = false;
      }
      this.selectedCol = col;
      col.hover = true;
      col.i = i;
      col.j = j;
    }
    console.log(this.selectedCol);
  }

  placeDeckLetter(letterIndex): void {
    if(this.selectedCol) {
      let letterValue = this.deck[letterIndex];
      if(letterValue == "*"){
        this.showWildcardSelect(letterIndex);
      }else {
        letterValue = this.deck[letterIndex];
        this.placeKnownLetter(letterIndex, letterValue);
      }

    }
  }

  placeKnownLetter(letterIndex, letter){
    this.boardLetters[this.selectedCol.i][this.selectedCol.j].letter = letter;
    this.selectedCol.new = true;
    let newLetter = {
      "letter": letter,
      "i":  this.selectedCol.i,
      "j":  this.selectedCol.j
    };
    this.placedLetters.push(newLetter);
    console.log('placedLetter', this.placedLetters);
    this.deck[letterIndex] = "";
    this.selectedCol = null;

  }

  showWildcardSelect(letterIndex){
      let profileModal = this.modalCtrl.create(WildcardSelectPage);
      profileModal.onDidDismiss(letterValue => {
        console.log('letterValue', letterValue);
        if(letterValue){
          this.placeKnownLetter(letterIndex, letterValue);
        }
      });
      profileModal.present();
  }

  removeTile(col, i, j): void {
    if(col.new){
      var emptySlotIndex = this.deck.indexOf("");
      let letter = this.boardLetters[i][j].letter;
      let n = letter.indexOf("*");
      if(n != -1){
        this.deck[emptySlotIndex] = '*';
      }else{
        this.deck[emptySlotIndex] = letter;
      }

      this.boardLetters[i][j].letter = " ";
      this.removeFromPlacedLetters(i, j);
      col.new = false;
    }
  }

  removeFromPlacedLetters(i, j): void {
    for(let k = 0; k < this.placedLetters.length; k++){
      if(this.placedLetters[k].i == i && this.placedLetters[k].j == j){
        this.placedLetters.splice(k, 1);
        break;
      }
    }
  }

  placeWord(){
    this.placingWord = true;
    this.apiService.placeWord(this.game.id, this.placedLetters).then((data) => {
      if(data['error']){
        let alert = this.alertCtrl.create({
          title: 'Klaida',
          subTitle: data['errorMessage'],
          buttons: ['Ok']
        });
        alert.present();
        this.placingWord = false;
      }else{
        this.setPlacedLettersOld();
        this.loadGameData();
        this.placingWord = false;
      }
    });
  }

  skipTurn(){
    this.skipingTurn = true;
    this.apiService.skipTurn(this.game.id).then((data) => {
      if(data['error']){
        let alert = this.alertCtrl.create({
          title: 'Klaida',
          subTitle: data['errorMessage'],
          buttons: ['Ok']
        });
        alert.present();
        this.skipingTurn = false;
      }else{
        this.loadGameData();
        this.skipingTurn = false;
      }
    });
  }

  setPlacedLettersOld(){
    console.log('this.board', this.board);
    console.log('this.placedLetters', this.placedLetters);
    for(let k = 0; k < this.placedLetters.length; k++){
        this.board[this.placedLetters[k].i][this.placedLetters[k].j].new = false;
    }
  }

  resetDeck(): void {
    this.deck = ["T","K","A","S","Y","Ž","A"];
  }

  getPointValue(letter){
    let points = this.utils.getPointValue(letter);
    if(points != 0)
      return points + '';
    else
      return "";
  }

  getPointValueBoard(boardLetter){
    if(boardLetter.wildcard)
      return "";

    let points = this.utils.getPointValue(boardLetter.letter);
    if(points != 0)
      return points + '';
    else
      return "";
  }
}
