import {Pipe} from '@angular/core';

@Pipe({
  name: 'letter'
})
export class Letter {
  transform(value, args) {
    if(value){
      value = value.replace("*", "");
      return value.toUpperCase();
    }
  }
}
